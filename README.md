Packages
=========

Ce rôle se charge d'installer une liste de paquet, fournie par une variable sous forme de liste.

Requirements
------------

N/A

Role Variables
--------------

```
packages_to_install: [git, vim, emacs]
```

Dependencies
------------

N/A

Example Playbook
----------------

    - hosts: servers
      roles:
        - packages-lqdn
      vars:
        packages_to_install: [cowsay, anarchism]

License
-------

GPLV3

Author Information
------------------

Fait par nono pour La Quadrature Du Net
